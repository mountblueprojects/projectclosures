function cacheFunction(callback) {
  if (typeof callback !== "function") {
    return [];
  }

  let cache = {};

  return function () {
    if (cache.hasOwnProperty([...arguments]) === false) {
      cache[[...arguments]] = callback(...arguments);
    }

    return cache[[...arguments]];
  };
}

module.exports = cacheFunction;
