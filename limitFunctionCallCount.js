function limitFunctionCallCount(callback, limit) {
  if (typeof callback !== "function" || typeof limit !== "number") {
    return [];
  }
  let invokedTimes = 0;

  return function () {
    if (invokedTimes < limit) {
      invokedTimes++;

      if (typeof callback(...arguments) === "undefined") {
        return null;
      }

      return callback(...arguments);
    }

    return [];
  };
}

module.exports = limitFunctionCallCount;
