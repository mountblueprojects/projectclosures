const cacheFunction = require("../cacheFunction");

function add(a, b) {
  return a + b;
}

const result = cacheFunction(add);
console.log(result(2, 5));
console.log(result(2, 5));
