const limitFunctionCallCount = require("../limitFunctionCallCount");

function add(a, b) {
  return a + b;
}

const result = limitFunctionCallCount(add, 3);

console.log(result(2, 3));

console.log(result(2, 3));

console.log(result(2, 3));

console.log(result(2, 3));

console.log(result(2, 3));
